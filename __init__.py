#! /usr/bin/python

from gi.repository import GObject as gobject
from gi.repository import Gtk as gtk
from gi.repository import Gdk as gdk
from gi.repository import Pango as pango

from zim.plugins import PluginClass
from zim.gui.mainwindow import MainWindowExtension
from zim.notebook import Path
from zim.gui.widgets import TOP_PANE,LEFT_PANE,BOTTOM_PANE,RIGHT_PANE

NOWHERE="nowhere"

def on_menu_position(menu, x,y,button):
	origin=button.get_window().get_origin()
	coords=button.get_allocation()
	return origin.x+coords.x,origin.y+coords.y+coords.height, True

def on_menu_position_up(menu, x,y,button):
	origin=button.get_window().get_origin()
	coords=button.get_allocation()
	return origin.x+coords.x,origin.y+coords.y-menu.get_allocation().height, True

class OpenDocumentsPlugin(PluginClass):
	plugin_info={
		'name'       :_('Open Documents Tab Bar'),  # T: plugin name
		'description':_("""This plugin adds a bar at the top of the zim window
with a tab bar that functionally replaces the pathbar. Each entry now
corresponds to an "open document" and will only go away when you
"close" it.

This plugin works well with the pathbar set to "page heirarchy" mode.
"""),
		# T: plugin description
		'author'     :'DeinonychusCowboy <blackline.silverfield@gmail.com>',
	}

	plugin_preferences=(
		('location', 'choice', _('Position in the window'), TOP_PANE, (
			(TOP_PANE, _('Top')), # T: Option for placement of plugin widgets
			(BOTTOM_PANE, _('Bottom')), # T: Option for placement of plugin widgets
		)),
		('close', 'choice', _('Close buttons on tabs'), RIGHT_PANE, (
			(NOWHERE, _('None (Middle Click to Close)')),  # T: Option for placement of plugin widgets
			(RIGHT_PANE, _('Right (Windows, others)')),  # T: Option for placement of plugin widgets
			(LEFT_PANE, _('Left (Mac, Unity)')),  # T: Option for placement of plugin widgets
		)),
		('tabsize', 'bool', _('Reduce Tab Height'), False),
	)

class ODPMainWindowExtension(MainWindowExtension):
	def __init__(self,plugin,window):
		super().__init__(plugin,window)
		self.manager=None
		self.on_preferences_changed(plugin.preferences)
		self.connectto(plugin.preferences,'changed',self.on_preferences_changed)
		self.connectto(window,'page-changed',self.on_page_opened)
		self.connectto(window.pageview,"key-press-event",self.on_page_keyboard)
		cssprovider=gtk.CssProvider()
		cssprovider.load_from_data(b"""
		.ODTB tab, .ODTB button {
			padding:0;
			margin:0;
		}
		.ODTB tab label, .ODTB.rightbutton tab label:last-child, .ODTB.leftbutton tab label:last-child{
			padding:0.5em 1em;
		}
		.ODTB.rightbutton tab label{
			padding:0.5em 0 0.5em 1em;
		}
		.ODTB.leftbutton tab label{
			padding:0.5em 1em 0.5em 0;
		}
		.ODTB.rightbutton tab image{
			padding: 0.5em 0.5em 0.5em 0;
		}
		.ODTB.leftbutton tab image{
			padding:0.5em 0 0.5em 0.5em;
		}
		""")
		screen = gdk.Screen.get_default()
		stylecontext = gtk.StyleContext()
		stylecontext.add_provider_for_screen(screen, cssprovider,gtk.STYLE_PROVIDER_PRIORITY_USER)

	def on_page_opened(self, ui, page):
		self.manager.open_page(page)

	def on_preferences_changed(self, preferences):
		pages=[]
		if self.manager is not None:
			pages=self.manager.get_open_pages()
			self.teardown()

		self.manager=OpenDocumentsTabManager(preferences,self.window,pages)

	def on_page_keyboard(self,view,key):
		# ignore navigation/arrow keys, numpad navigation/arrow keys, and modifier keys
		if key.keyval not in range(0xff50, 0xff58) and key.keyval not in range(0xff95, 0xff9d) and key.keyval not in range(0xffe1,0xffef) and key.keyval not in range(0xfe01,0xfe14) and key.keyval not in range(0xff7e,0xff80) and key.keyval not in range(0xff14,0xff15):
			self.manager.make_permanent(self.window.pageview.page)

	def destroy(self):
		self.teardown()

	def teardown(self):
		self.manager.teardown()
		self.manager=None

class TabManagerContainer(gtk.HBox):
	pass

class OpenDocumentsTabManager:
	def __init__(self,preferences,window,pages):
		self.window=window
		self.widget=OpenDocumentsTabBar(preferences)
		self.widget.set_show_border(False)
		self.widget.set_scrollable(True)
		self.widget.popup_enable()
		gtk_pos=gtk.PositionType.TOP
		if preferences["location"]==BOTTOM_PANE:
			gtk_pos=gtk.PositionType.BOTTOM
		self.widget.set_property("tab-pos",gtk_pos)

		self.menu=gtk.Button()
		menuimage=gtk.Image()
		menuimage.set_from_pixbuf(self.menu.render_icon(gtk.STOCK_GO_DOWN,gtk.IconSize.MENU))
		self.menu.set_image(menuimage)
		self._contextmenu=gtk.Menu()
		item1=gtk.MenuItem("Close Current Tab")
		item2=gtk.MenuItem("Close All But Current")
		item3=gtk.MenuItem("Close Tabs to the Left")
		item4=gtk.MenuItem("Close Tabs to the Right")
		item5=gtk.MenuItem("Make Current Tab Permanent")
		item6=gtk.MenuItem("Make Current Tab Transient")
		item1.connect("activate",self.on_close_clicked)
		item2.connect("activate",self.on_close_both_clicked)
		item3.connect("activate",self.on_close_left_clicked)
		item4.connect("activate",self.on_close_right_clicked)
		item5.connect("activate",self.on_permanent_clicked)
		item6.connect("activate",self.on_transient_clicked)
		self._contextmenu.attach(item1,0,1,0,1)
		self._contextmenu.attach(item2,0,1,1,2)
		self._contextmenu.attach(item3,0,1,2,3)
		self._contextmenu.attach(item4,0,1,3,4)
		self._contextmenu.attach(item5,0,1,4,5)
		self._contextmenu.attach(item6,0,1,5,6)
		self._contextmenu.show_all()

		# I'm a sinner. Can't find a better way to get the correct container to add this to than using a private property.
		# It is both wonderful and horrifying that python allows me to just do this.
		self.container=self.window._zim_window_bottom_paned.get_child1()

		self.hbox=TabManagerContainer()
		self.hbox.pack_start(self.menu,False,False,5)
		self.hbox.pack_start(self.widget,True,True,0)
		self.container.pack_start(self.hbox, False,True,0)
		self.hbox.show_all()

		if preferences["tabsize"]:
			context = self.hbox.get_style_context()
			context.add_class("ODTB")

		if preferences["location"]==TOP_PANE or preferences["location"]==RIGHT_PANE:
			self.container.reorder_child(self.hbox, 0)

		# T: widget label
		self.widget.show_all()

		self._pages={}
		self._inverse_pages={}
		self._transient_page=None

		if pages is not None:
			for page in pages:
				self.open_page_silently(page)

		if self.window.pageview.page is not None:
			self.open_page(self.window.pageview.page)

		self.sync_page()

		self.__handlers=[]
		self.__menu_handlers=[]
		self.__handlers.append(self.widget.connect("page-removed",self.on_page_removed))
		self.__handlers.append(self.widget.connect("page-reordered",self.on_page_reordered))
		self.__handlers.append(self.widget.connect("switch-page",self.on_page_switched))
		self.__handlers.append(self.widget.connect("permanence",self.on_explicit_permanence))
		self.__menu_handlers.append(self.menu.connect("button-press-event",self.on_menu))

	def on_close_clicked(self,menuitem):
		self.widget.close_current()
	def on_close_left_clicked(self,menuitem):
		self.widget.close_left()
	def on_close_right_clicked(self,menuitem):
		self.widget.close_right()
	def on_close_both_clicked(self,menuitem):
		self.widget.close_both()
	def on_permanent_clicked(self,menuitem):
		self.make_permanent(self.window.pageview.page)
	def on_transient_clicked(self,menuitem):
		self.make_transient(self.window.pageview.page)

	def open_page(self,page):
		if page not in self._pages:
			self.open_page_silently(page)
			self.widget.set_current_page(self._pages[page])
			self.make_transient(page)
		else:
			self.widget.set_current_page(self._pages[page])

	def open_page_silently(self,page):
		if page not in self._pages:
			self.widget.add_document_tab(page.name,page.basename)
			new_index=self.widget.get_n_pages()-1
			self._pages[page]=new_index
			self._inverse_pages[new_index]=page
			self.widget.show_all()

	def make_transient(self,page):
		if self._transient_page is not None:
			# a transient page already exists, so remove the old one and replace it with this one
			index=self._pages[self._transient_page]
			self.widget.remove_page(index)
		self._transient_page=page
		self.widget.make_transient(self._pages[self._transient_page])

	def make_permanent(self,page):
		if self._transient_page is not None and self._transient_page == page:
			self.widget.make_permanent(self._pages[page])
			self._transient_page=None

	def get_open_pages(self):
		return self._inverse_pages.values()

	def on_explicit_permanence(self,sender,page_num):
		self.make_permanent(self._inverse_pages[page_num])

	def on_menu(self,sender,event):
		if self.widget.get_property("tab-pos")==gtk.PositionType.TOP:
			self._contextmenu.popup(None,None,on_menu_position,self.menu,event.button,event.time)
		else:
			self._contextmenu.popup(None,None,on_menu_position_up,self.menu,event.button,event.time)

	def on_page_removed(self,notebook,child,page_num):
		removed_page=self._inverse_pages[page_num]
		if self._transient_page==removed_page:
			self._transient_page=None
		del self._inverse_pages[page_num]
		del self._pages[removed_page]

		new_inverse_pages={}
		changed_pages=[]
		for key in self._inverse_pages:
			if key>page_num:
				new_inverse_pages[key-1]=self._inverse_pages[key]
				changed_pages.append(self._inverse_pages[key])
			else:
				new_inverse_pages[key]=self._inverse_pages[key]
		self._inverse_pages=new_inverse_pages

		for page in changed_pages:
			self._pages[page]=self._pages[page]-1

		self.sync_page()

	def on_page_reordered(self,notebook,child,page_num):
		path=Path(child.get_name())
		old_page_num=self._pages[path]
		shift=[]
		mod=0
		left=0
		right=0
		if old_page_num>page_num:
			# shift intermediate pages right
			mod=1
			left=page_num
			right=old_page_num-1
		elif old_page_num<page_num:
			# shift intermediate pages left
			mod=-1
			left=old_page_num+1
			right=page_num
		else:
			return

		cur_page_num=left
		while cur_page_num<=right:
			shift.append(self._inverse_pages[cur_page_num])
			cur_page_num+=1
		for page in shift:
			newindex=self._pages[page]+mod
			self._pages[page]=newindex
			self._inverse_pages[newindex]=page
		self._pages[path]=page_num
		self._inverse_pages[page_num]=path

		self.sync_page()

	def open_ui_page(self,page_num):
		self.window.open_page(self._inverse_pages[page_num])
		#if self._inverse_pages[page_num] == self._transient_page:
		#	self.make_permanent()

	def on_page_switched(self,notebook,dummy_widget,page_num):
		if page_num not in self._inverse_pages:
			return
		if self.window.pageview.page!=self._inverse_pages[page_num]:
			self.open_ui_page(page_num)

	def sync_page(self):
		current_tab_page=self.widget.get_nth_page(self.widget.get_current_page())
		if current_tab_page is not None and self.window.pageview.page!=Path(current_tab_page.get_name()):
			self.open_ui_page(self.widget.get_current_page())

	def teardown(self):
		self.widget.teardown()
		for child in self.container.get_children():
			if type(child) is TabManagerContainer:
				self.container.remove(child)
		for handler in self.__handlers:
			self.widget.disconnect(handler)
		for handler in self.__menu_handlers:
			self.menu.disconnect(handler)
		self.widget=None
		self.menu=None
		self.hbox=None
		self._contextmenu=None

class OpenDocumentsTabBar(gtk.Notebook):
	# define signals we want to use - (closure type, return type and arg types)
	__gsignals__ = {
		'permanence': (gobject.SIGNAL_RUN_LAST, None, (int,)),
	}

	def __init__(self,preferences):
		super().__init__()
		self._preferences=preferences
		self.__handlers=[]
		self.__handlers.append(self.connect("page-added",self.on_page_added_removed))
		self.__handlers.append(self.connect("page-removed",self.on_page_added_removed))

	def teardown(self):
		for x in self.__handlers:
			self.disconnect(x)

	def add_document_tab(self, pathname, labeltext):
		child = gtk.HBox()
		child.set_name(pathname)
		label = gtk.HBox()
		text = gtk.Label(labeltext)

		if self._preferences["close"] != NOWHERE:
			button = gtk.EventBox()
			button.set_property("visible-window", False)
			button_image = gtk.Image()
			button_image.set_from_stock(gtk.STOCK_CLOSE, gtk.IconSize.MENU)
			button.add(button_image)
			button.connect("button-press-event", self.on_close_clicked, child)
			button.show_all()

			first_label=text
			second_label=button
			first_expand=True
			second_expand=False

			if self._preferences["close"]==LEFT_PANE:
				context = self.get_style_context()
				context.add_class("leftbutton")
				first_label=button
				second_label=text
				first_expand=False
				second_expand=True
			else:
				context = self.get_style_context()
				context.add_class("rightbutton")
			label.pack_start(first_label, first_expand, first_expand,0)
			label.set_spacing(6)
			label.pack_end(second_label, second_expand, second_expand,0)
		else:
			label.pack_start(text,True,True,0)

		text.show_all()
		label_box=gtk.EventBox()
		label_box.add(label)
		label_box.set_property("visible-window",False)
		label_box.connect("button_press_event",self.on_tab_clicked,child)
		label_box.show_all()
		self.append_page(child, tab_label=label_box)
		self.set_menu_label_text(child,labeltext)
		self.set_tab_reorderable(child,True)

	def make_transient(self,index):
		label=self.get_text_label(index)
		font_description = pango.FontDescription()
		font_description.set_style(pango.Style.ITALIC)
		label.modify_font(font_description)

	def make_permanent(self,index):
		label=self.get_text_label(index)
		font_description = pango.FontDescription()
		font_description.set_style(pango.Style.NORMAL)
		label.modify_font(font_description)

	def on_tab_clicked(self,widget,event,child):
		if event.button==2:
			self.on_close_clicked(widget,event,child)
		elif event.type==gdk.EventType._2BUTTON_PRESS:
			self.emit('permanence',self.get_current_page())

	def close_current(self):
		if self.get_n_pages()>1:
			self.remove(self.get_nth_page(self.get_current_page()))

	def close_left(self):
		marker=self.get_current_page()
		if self.get_n_pages()>1 and marker>0:
			for x in range(marker-1,-1,-1):
				self.remove(self.get_nth_page(x))

	def close_right(self):
		marker=self.get_current_page()
		if self.get_n_pages()>1 and marker<self.get_n_pages():
			for x in range(self.get_n_pages()-1,marker,-1):
				self.remove(self.get_nth_page(x))

	def close_both(self):
		self.close_left()
		self.close_right()

	def on_close_clicked(self,widget,event,child):
		if ( event.button==2 or event.button==1 ) and self.get_n_pages()>1:
			self.remove(child)

	def get_close_button(self,pindex):
		if self._preferences["close"]==RIGHT_PANE:
			return self.get_tab_label(self.get_nth_page(pindex)).get_children()[0].get_children()[1]
		elif self._preferences["close"]==LEFT_PANE:
			return self.get_tab_label(self.get_nth_page(pindex)).get_children()[0].get_children()[0]
		else:
			return None

	def get_text_label(self,pindex):
		if self._preferences["close"]==RIGHT_PANE:
			return self.get_tab_label(self.get_nth_page(pindex)).get_children()[0].get_children()[0]
		elif self._preferences["close"]==LEFT_PANE:
			return self.get_tab_label(self.get_nth_page(pindex)).get_children()[0].get_children()[1]
		else:
			return self.get_tab_label(self.get_nth_page(pindex)).get_children()[0].get_children()[0]

	def on_page_added_removed(self,notebook,dummy,page_num):
		if self.get_close_button(0) is not None:
			if self.get_n_pages()>1:
				self.get_close_button(0).show()
			else:
				self.get_close_button(0).hide()
