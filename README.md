Zim Open Documents Tab Bar
==========================

This is a plugin for [Zim Desktop Wiki](http://www.zim-wiki.org/) that adds a tab bar above (or below)
the editor view. This tab bar is similar to the existing pathbar at the top of the window, but works
more like a traditional tabbed document interface; you can now treat Zim as though it has multiple
documents "open", switch between them easily without them moving around in the pathbar, and you can
"close" documents when you're done with them.

Since this renders much of the pathbar's functionality vestigial, I recommend setting the pathbar to
"page heirarchy mode" (View > Pathbar) to give yourself a nice breadcrumb-like layout in addition to
the tab bar. Or, you can always disable it entirely from the same menu.

The tab bar may be placed above or below the editor view; unfortunately, placing it to the left or right
is not easily possible in Zim's layout. You may also configure where the close icon appears on tabs
(when there are multiple tabs open), or close tabs by middle-click. Tabs can be re-ordered and behave
well at various window sizes and combinations of side panes.

The default tab for newly opened documents is a "transient tab", indicated by italics in the tab title. A transient tab will close when you open another document. This allows you to click through 10 documents without 10 tabs opening up. Editing the contents of the document, or double-clicking on the tab, will transform it into a "permanent tab" that will stay open when you open other documents.

To install, clone this repo to ~/.local/share/zim/plugins/zim-open-documents-bar, and then open Zim
and enable it through the plugins interface.
